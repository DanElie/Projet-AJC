# Projet-AJC
Projet réalisé par Nathan Hieu, Brian Goncalves et Daniel Eliahou Ontiveros

Dépôt du code lié au projet fil rouge d'AJC

## La stack Docker :

La stack Docker est présente dans le répertoire éponyme.
Elle contient :
- notre fichier docker-compose.yml utilisé afin de mettre en place nos services Docker.
- un fichier prometheus.yml pour configurer le conteneur Prometheus, et plus précisément les jobs de monitoring a accomplir.
- un répertoire grafana qui contient des fichiers de configurations :
  - Un fichier datasource.yml, qui va permettre a Grafana de se mettre en lien avec Prometheus
  - Des fichiers .yml et .json pour configurer l'affichage des Dashboards, présents dans le répertoire du même nom.

### Le Monitoring (Prometheus, Grafa)

Prometheus est mis en place de cette façon :

```
version: "3.7"

services:

  prometheus:
    image: bitnami/prometheus:latest
    restart: always
    expose:
      - 9090:9090
    networks: 
      - prometheus_network
    volumes:
      - prometheus_data:/prometheus
      - ./prometheus.yml:/etc/prometheus/prometheus.yml:ro
    depends_on:
      - cadvisor
```

Nous téléchargeons l'image prometheus, exposons le port 9090 et créons un volume afin d'enregistrer les données.
Par ailleurs, on lui transmets le fichier prometheus.yml pour configurer les jobs de scraping permettant au conteneur de récupérer les données pertinentes des sources indiquées.

Ensuite, Grafana est mise en place comme ceci:

```
  grafana:
    image: grafana/grafana-oss
    ports:
      - 3000:3000
    user: '104'
    networks:
      - prometheus_network
      - default
    volumes:
      - grafana_data:/var/lib/grafana
      - ./grafana/dashboards:/etc/grafana/provisioning/dashboards
      - ./grafana/datasources:/etc/grafana/provisioning/datasources
      
```
De même que pour Prometheus, on télécharge l'image, on crée un volume pour stocker les données, mais ici, l'exposition des ports  se fait avec la directives "ports" qui va permettre d'accéder à l'interface de Grafana depuis l'extérieur.
Ici encore, on utilise la directive "volumes" pour transmettre les fichiers de configurations mentionnées plus tôt au conteneur Grafana.